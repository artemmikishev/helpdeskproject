package ru.edu.helpdesk.service;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.edu.helpdesk.entity.Title;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.form.UserEditForm;
import ru.edu.helpdesk.repository.TicketRepository;
import ru.edu.helpdesk.repository.TitleRepository;
import ru.edu.helpdesk.repository.UserRepository;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final TitleRepository titleRepository;

    private final TicketRepository ticketRepository;

    public AdminServiceImpl(UserRepository userRepository,
                            PasswordEncoder passwordEncoder,
                            TitleRepository titleRepository,
                            TicketRepository ticketRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.titleRepository = titleRepository;
        this.ticketRepository = ticketRepository;
    }

    /**
     * Метод allUsers() выводит список всех пользователей сохраненных в БД
     */
    @Override
    public List<User> allUsers() {
        return userRepository.findAll();
    }

    /**
     * Метод AdminServiceImpl#saveUser(User user) сохраняет нового пользователя в БД
     *
     * @param user новый пользователь для вставки в БД
     */
    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    /**
     * Метод AdminServiceImpl#getUserByUsername(String username) ищет пользователя по логину
     *
     * @param username логин искомого пользователя
     * @return возвращает нужного нам пользователя или NULL если пользователь не найден
     */
    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByLogin(username);
    }

    /**
     * Метод ищет пользователя по id
     */
    @Override
    public User getUserById(Long id) {
        return userRepository.getById(id);
    }

    /**
     * Метод обновляет информацию о пользователе
     */
    @Override
    public void updateUser(UserEditForm userEditForm) {
        User user = userRepository.getById(userEditForm.getId());
        user.setLogin(userEditForm.getLogin());
        user.setFirstName(userEditForm.getFirstName());
        user.setLastName(userEditForm.getLastName());
        user.setRole(userEditForm.getRole());
        if (userEditForm.getChangePassword() != null && userEditForm.getChangePassword()) {
            final String hashedPassword = passwordEncoder.encode(userEditForm.getNewPassword());
            user.setPassword(hashedPassword);
        }
        userRepository.save(user);
    }

    /**
     * Метод allTitles() выводит список всех категорий сохраненных в БД
     */
    @Override
    public List<Title> allTitles() {
        return titleRepository.findAllByOrderByNameAsc();
    }

    /**
     * Метод ищет категорию по id
     */
    @Override
    public Title getTitleById(Long id) {
        return titleRepository.getById(id);
    }

    /**
     * Метод сохраняет категорию в БД
     */
    @Override
    public void saveTitle(Title title) {
        titleRepository.save(title);
    }

    /**
     * Метод удаляет категорию из БД
     */
    @Override
    public void deleteTitleById(Long id) {
        int count = ticketRepository.countByTitleId(id);
        if (count > 0) {
            throw new RuntimeException("Удалить невозможно, данная категория используется!");
        }
        titleRepository.deleteById(id);
    }
}
